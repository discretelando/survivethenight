# survivethenight
Follow the events of an unknown protagonist as they try to unravel the mystery of a murder that has just taken place. The events of this game and therefore this night, all take place inside of one home. With six suspects at the beginning of this adventure, can you successfully navigate this text based murder mystery game? Or will you succumb to the lies of a conniving murderer and end up his... or her next victim? Good luck my friend. You're definitely going to need it.

Directions:

1. To run the game type "make run". Or simply "make" into the terminal and then hit enter

2. The previous command creates an executable file called runGame

3. Then you need to simply run the program. In linux and macOS you will need to enter "./runGame"

4. Enjooy our game :)