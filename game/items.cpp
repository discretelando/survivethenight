#include <stdio.h>
#include <algorithm>
#include <vector>
#include "items.hpp"

Items::Items(){
	this->count = 0;
}

int Items::itemsCount(){
	return this->count;
}

bool Items::addItem(std::string s){
	this->inventory.push_back(s);
	this->count++;
	return 1;
}

bool Items::dropItem(std::string s){
	if (std::find(this->inventory.begin(), this->inventory.end(),s)!= this->inventory.end()){
		try{
			this->inventory.erase(std::remove(this->inventory.begin(), this->inventory.end(), s), this->inventory.end());
			this->count--;
			return true;
		}
		catch(...){
			return false;
		}
	}
	else return false;
}

bool Items::checkItem(std::string s){
	if (std::find(this->inventory.begin(), this->inventory.end(),s)!= this->inventory.end()){
		return true;
	}
	else
		return false;
}

