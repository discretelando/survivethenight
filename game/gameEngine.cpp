#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include "items.hpp"
#include "events.hpp"
#include <chrono>
#include <thread>
#include "character.hpp"

	bool DoesFileExist (const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    } 
    }

int main(){
	Character character;
	Path path;
	Events event;
	Suspects suspect;
	bool game = true;
	bool valid = false;
	std::string user_input = "";
	std::string temp = "";
	std::string action = "";
	std::string choice = "";
	std::string decision = "";
	std::string name = "";
	std::string save = "";
	std::vector<int>::size_type size = 0;
	int len = 0;
	int room = 0;
	int response = 0;


    if(DoesFileExist("gamesave")){
    	std::cout << ("A game save file has been detected. Would you like to use it (y), or start over? (n)");
    	std::cin >> save;
    	if(save == "y"){
    		ifstream infile("gamesave");
    		if (infile.good())
    		{
    			string sLine;
    			getline(infile, sLine);
    			cout << sLine << endl;
    			std::string delim = ",";
    			int pos = sLine.find(delim);
    			name = sLine.substr(0, pos);
    			sLine.erase(0, pos + delim.length());
    			std::string know = sLine.substr(0, sLine.find(delim));
    			int iknow = std::stoi(know);
    			character.saveKnowledge(iknow);
				event.printEvent("living");
				std::cout << "Welcome back "<< name << " your current knowledge is " << character.getKnowledge() << "\n\n";
    		}
		  	infile.close();
    	}
    	else if(save == "n"){
    		remove("gamesave");
    		ofstream newsave;
    		std::cout << "Please Enter a Name for your Character: ";
    		std::cin >> name;
    		newsave.open("gamesave");
    		newsave<<name<<",0";
    		newsave.close();
        	event.printIntro();
			std::this_thread::sleep_for(std::chrono::milliseconds(7500));
			event.printEvent("commands");
			event.printEvent("commands_1");
			std::this_thread::sleep_for(std::chrono::milliseconds(3500));
			event.printEvent("living");
			std::cout << "Welcome "<< name << " the house rules are above everything, never forget them...\nWhat would you like to do?\n\n";
    	}
    	else{
    		std::cout << "Invlaid Response. Goodbye\n";
    		game = false;
    	}
    }
    else{
    	std::cout << "Please Enter a Name for your Character: ";
    	std::cin >> name;
    	ofstream newsave;
		newsave.open("gamesave");
		newsave<<name<<",0";
		newsave.close();
    	event.printIntro();
		std::this_thread::sleep_for(std::chrono::milliseconds(7500));
		event.printEvent("commands");
		event.printEvent("commands_1");
		std::this_thread::sleep_for(std::chrono::milliseconds(3500));
		event.printEvent("living");
		std::cout << "Welcome "<< name << " the house rules are above everything, never forget them...\nWhat would you like to do?\n\n";

    }
	while(game){
		std::getline(cin, user_input);
		len = user_input.length();
		for(int i = 0; i < len; i++){
			if((user_input[i] < 'a' || user_input[i] > 'z') && user_input[i] != ' '){
				std::cout << "please enter lower case input\n\n";
				valid = false;
				break;
			}
			else valid = true;
		}

		if(valid == false)
			continue;
		std::vector<std::string> tokens;
		std::stringstream ss(user_input);

		while(getline(ss, temp, ' ')){
			tokens.push_back(temp);
		}

		if(tokens.size() > 2){
			std::cout << "you attempt to do too much, try fewer \n\n";
			continue;
		}

		if(tokens.size() < 2){
			std::cout << "your intent isn't clear. Make sure you have an action and a choice\n\n";
			continue;
		}

		action = tokens[0];
		choice = tokens[1];

		if(action == "inspect"){
			if(path.validInspect(character.getLocation(), choice)){
			character.setKnowledge();
			event.inspect(choice);
		}
			else{
				std::cout << "you can't find what you're looking for\n\n";
				continue;
			}
		}
		else if(action == "take"){
			if(path.validTake(character.getLocation(), choice))
			event.take(choice);
			else{
				character.setKnowledge();
				std::cout << "you can't find what you're looking for\n\n";
				continue;
			}

		}
		else if(action == "save" && choice == "game"){
    		remove("gamesave");
    		ofstream newsave;
    		newsave.open("gamesave");
    		newsave<<name<<","<<character.getKnowledge();
			std::cout << "GAME SAVED\n";
		}
		else if(action == "drop"){
			if(event.drop(choice)){
				std::cout << "Dropped: " << choice << "\n\n";
				continue;
			}
			else
				std::cout << choice << " not in inventory\n\n";
		}

		else if(action == "interview"){
			valid = true;
			while(valid){
				suspect.printOptions(choice);
				std::getline(cin, decision);
				int length = decision.length();
				if(length != 1){
					std::cout << "you don't know what to say... try something else\n\n";
					continue;	
				}
				for(int i = 0; i < length; i++){
					if(decision[i] < 49 || decision[i] > 57){
						valid = false;
						break;
					}
					else
						valid = true;
				}
				if(valid == true){
					stringstream pick(decision);
					pick >> response;
					if(response >= 1 && response <= 4){
					if(path.validInterview(character.getLocation(), choice)){
						suspect.interviewSuspect(choice, response);
						character.setKnowledge();
						break;
					}
					else{
						valid = false;
						continue;
					}
				}
			}
				else{
					std::cout << "words are not your strong suit. try something else\n\n";
					continue;
				}
			}
		}

		else if(action == "enter"){
			if(choice == "living")
				room = 0;
			else if(choice == "death"){
				room = 1;
			}
			else if(choice == "kitchen")
				room = 2;
			else if(choice== "dining")
				room = 3;
			else if(choice == "bathroom")
				room = 4;
			else if(choice == "lounge")
				room = 5;
			else if(choice == "master")
				room = 6;
			else if(choice == "guest")
				room = 7;
			if(path.checkConnections(character.getLocation(), room)){
				character.setLocation(room);
				event.printEvent(choice);
			}
			else{
				std::cout << "teleportation has not been invented yet. try another room\n\n";
			}
		}

		else if(action  == "print"){
			if(choice == "instructions"){
				event.printEvent("instructions");
			}
			else if(choice == "rooms"){
				std::vector<int> openRooms = path.availablePaths(character.getLocation());
				std::cout << "Available Rooms: ";
				for(auto i : openRooms){
					if(i == 0)
						std::cout << "living ";
					else if(i == 1)
						std::cout << "death ";
					else if(i == 2)
						std::cout << "kitchen ";
					else if(i == 3)
						std::cout << "dining ";
					else if(i == 4)
						std::cout << "bathroom ";
					else if(i == 5)
						std::cout << "lounge ";
					else if(i == 6)
						std::cout << "master ";
					else if(i == 7)
						std::cout << "guest ";
				}
			}
		}

		else if(action == "accuse"){
			if(character.getKnowledge() < 20){
				std::cout << "You've been murdered\n\n";
				event.printDeath();
				game = false; 
			}
			else if(character.getLocation() != 1){
				std::cout << "WE WARNED YOU!\nThe murder laid in wait until you were alone in the room.\nAnd alas, you were their next victim.\nYou should learn to play by the rules old sport.\n";
				event.printDeath();
				game = false;
			}
			else if(choice != "kayla"){
				std::cout << "You accused an innocent person.\n The murderer got away... but not after murdering you\n\n";
				event.printDeath();
				game = false;
			}
			else if(choice == "kayla"){
				event.printWin();
				std::cout << "THANK YOU FOR PLAYING!\n";
				game = false;
			}
		}

		else 
			std::cout << "That is not allowed within these walls\n";		
 	}
 	event.printEnd();
	return 0;
}