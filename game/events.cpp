#include <iostream>
#include <chrono>
#include <thread>
#include "events.hpp"

using namespace std;

// Print introduction.
void Events::printIntro() {
    printEvent("intro");
}

// Print the end game screen.
void Events::printEnd() {
    string x[5][1] = {
        {"***** *   * *****    ***** *   * **** "},
        {"  *   *   * *        *     **  * *   *"},
        {"  *   ***** *****    ***** * * * *   *"},
        {"  *   *   * *        *     *  ** *   *"},
        {"  *   *   * *****    ***** *   * **** "}};
    int ctr = 0;
    while (ctr < 5) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        cout << x[ctr][0] << endl;
        ctr += 1;
    }
}

void Events::printWin() {
    string x[5][1] = {
        {"*   * * *** *   *    ****   *    *  *****  *   *  *  *   * ***** ****"},
        {"  *   *   * *   *    *      *    *  *   *  *   *  *  *   * *     *   *"},
        {"  *   *   * *   *    *****  *    *  *  *   *   *  *  *   * ***** *   *"},
        {"  *   *   * *   *        *  *    *  *   *   * *   *   * *  *     *   *"},
        {"  *   ***** *****    *****  ******  *    *   *    *    *   ***** ****"}};
    int ctr = 0;
    while (ctr < 5) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        cout << x[ctr][0] << endl;
        ctr += 1;
    }
}

void Events::printDeath() {
    string x[5][1] = {
        {"**************************************************************************"},
        {"**************************************************************************"},
        {"**************************************************************************"},
        {"******************************DEATH***************************************"},
        {"**************************************************************************"}};
    int ctr = 0;
    while (ctr < 5) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        cout << x[ctr][0] << endl;
        ctr += 1;
    }
}
// Return a short description of object.
void Events::inspect(string obj) {
inventory.itemText(obj);
}

// Take item if possible.
bool Events::take(string item) {
    this->inventory.addItem(item);
    cout << "Inventory count: " << this->inventory.itemsCount() << "\n";
    return 1; // return 1 to indicate user is currently holding something.
}

// Drop item if user is holding it. 
bool Events::drop(string item) { 
    if(this->inventory.dropItem(item)){
    cout << "Inventory count: " << this->inventory.itemsCount() << "\n";
    return true; // return 0 to indicate user is no longer holding anything.
}
    else
    return false;
}

// Ask the other characters questions.
bool Events::interview(string q) {    
}

// Enter the specified room if possible.
bool Events::enter(int room) {
    if (this->path.checkConnections(current_pos, room)) {
        current_pos = room;
        return 1; // return 1 if valid to enter room.
    }
    return 0;
}
