#ifndef SUSPECTS_H_
#define SUSPECTS_H_
#include <stdio.h>
#include <string>

class Suspects{
private:
int bryant_currentLocation;
int mary_currentLocation;
int tracy_currentLocation;
int richard_currentLocation;
int otis_currentLocation;
int kayla_currentLocation;

std::string bryant[4][2] = {
	{"1) DESCRIPTION\n", "A normal looking enough person.\nMost likely over 30, slightly balding and he doesn't hide it well.\nHe's dressed in a gray suit with a blue tie and a sweater vest.\nBy his wedding ring you assume he's married.\n"},
	{"2) Where are you from?\n", "I was born in Chicago\n"},
	{"3) What do you do?\n", "I am a mortgage broker\n"},
	{"4) How old are you?\n","I am 32 years old\n"}
};
std::string mary[4][2] = {
	{"1) DESCRIPTION\n", "A very modestly dressed woman with a stark personality.\nAppears to be a little older and unwed.\nShe has black hair and wears a bright pink dress.\n"},
	{"2) Where are you from\n","I was born in Eugene\n"},
	{"3) What do you do?\n", "I am a primary care physician\n"},
	{"4) How old are you?\n","I am 41 years old\n"}
};
std::string tracy[4][2] = {
	{"1) DESCRIPTION\n", "A young black man trying to make his way in the world.\nHis original neighboorhood was too sketchy so he had to move to his auntie and uncle in Bel-Air.\nHe wears bright colors and appears to be very forward.\n"},
	{"2) Where are you from?\n","I was born in West Philadelphia\n"},
	{"3) What do you do?\n", "I am a veterinary technician\n"},
	{"4) How old are you?\n", "I am 29 years old\n"}
};
std::string richard[4][2] = {
	{"1) DESCRIPTION\n", "By far the oldest person in the house.\nHe has seemed suspiscious since you first met him.\nHe seems to keep to himself and is aggressive with people he does not know.\n"},
	{"2) Where are you from?\n", "I was born in Austin\n"},
	{"3) What do you do?\n", "I am a 2 star michellin decorated chef\n"},
	{"4) How old are you?\n", "I am 56 years old\n"}
};
std::string otis[4][2] = {
	{"1) DESCRIPTION\n", "He seems like a young, sharp individual.\nHe dresses well and takes care of himself.\nHe can't be much older than 20, but he seems like he might be hiding something.\n"},
	{"2) Where are you from?\n", "I was born in San Francisco\n"},
	{"3) What do you do?\n", "I am a math professor\n"},
	{"4) How old are you?\n", "I am 27 years old\n"}
};
std::string kayla[4][2] = {
	{"1) DESCRIPTION\n", "She appears to be a younger woman, most likely in her 20s.\nShe is very peppy and happy to answer questions.\nOut of everyone, she appears to be exceptionally trustworthy.\n"},
	{"2) Where are you from?\n","I was born in San Jose\n"},
	{"3) What do you do?\n", "I am a web developer\n"},
	{"4) How old are you?\n", "I am 23 years old\n"}
};

public:
	void printOptions(std::string suspect);
	void interviewSuspect(std::string suspect, int decision);
	void suspectsLocation(int characterLocation);
	int getSuspectsLocation(std::string s);
	void setLocation(std::string suspect, int locale);
};

#endif