#ifndef ITEMS_H_
#define ITEMS_H_
#include <stdio.h>
#include <string>
#include <vector>
#include "character.hpp"

class Items{
private:
	std::vector<std::string> inventory;
	int count;

public:
	Items();
	int itemsCount(); //gets the count of total items

	bool addItem(std::string s); //adds items and returns true if successful

	bool dropItem(std::string s); //drops item... returns true if successful

	bool checkItem(std::string s); //returns true if item is found

	void itemText(std::string s);
};

#endif
