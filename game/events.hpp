#include <iostream>
#include <string>
#include "items.hpp"
#include "path.hpp"

using namespace std;

#ifndef EVENTS_HPP_
#define EVENTS_HPP_

class Events {
private:
    int current_pos;
    Items inventory;
    Path path;
    
public:
    // Print introduction. 
    void printIntro();

    // Print the end game screen.
    void printEnd();

    void printWin();

    void printDeath();
 
    // Print event.
    void printEvent(string s); 

    // Print a short description of object.
    void inspect(string obj);

    // Take item if possible.
    bool take(string item);

    // Drop item if user is holding it.
    bool drop(string item);

    // Ask the other characters questions.
    bool interview(string q);

    // Enter the specified room if possible.
    bool enter(int room);
};

#endif
