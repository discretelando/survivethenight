#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <thread>
#include "suspects.hpp"
#define RAND_SIZE 9

void Suspects::printOptions(std::string suspect){
	std::cout << "Hmm maybe I can try asking " << suspect << " these?\n";
	for(int i = 0; i < 4; i++){
		if(suspect == "bryant")
			std::cout << this->bryant[i][0];
		else if(suspect == "mary")
			std::cout << this->mary[i][0];
		else if(suspect == "tracy")
			std::cout << this->tracy[i][0];
		else if(suspect == "richard")
			std::cout << this->richard[i][0];
		else if(suspect == "otis")
			std::cout << this->otis[i][0];
		else if(suspect == "kayla")
			std::cout << this->kayla[i][0];
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
}

void Suspects::interviewSuspect(std::string suspect, int decision){
	--decision;
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		if(suspect == "bryant"){
			std::cout << this->bryant[decision][1];
		}
		else if(suspect == "mary"){
			std::cout << this->mary[decision][1];
		}
		else if(suspect == "tracy"){
			std::cout << this->tracy[decision][1];
		}
		else if(suspect == "richard"){
			std::cout << this->richard[decision][1];
		}
		else if(suspect == "otis"){
			std::cout << this->otis[decision][1];
		}
		else if(suspect == "kayla"){
			std::cout << this->kayla[decision][1];
		}
	else
		std::cout << "who are you talking to?\n";
}

void Suspects::suspectsLocation(int characterLocation){
	int random = 0;
	if(characterLocation == 1){
		this->bryant_currentLocation = 1;
		this->mary_currentLocation = 1;
		this->tracy_currentLocation = 1;
		this->richard_currentLocation = 1;
		this->otis_currentLocation = 1;
		this->kayla_currentLocation = 1;
	}
	else{
		random = rand() % RAND_SIZE;
		this->bryant_currentLocation = random;
		random = rand() % RAND_SIZE;
		this->mary_currentLocation = random;
		random = rand() % RAND_SIZE;
		this->tracy_currentLocation = random;
		random = rand() % RAND_SIZE;
		this->richard_currentLocation = random;
		random = rand() % RAND_SIZE;
		this->otis_currentLocation = random;
		random = rand() % RAND_SIZE;
		this->kayla_currentLocation = random;
	}
}

int Suspects::getSuspectsLocation(std::string s){
	if(s == "bryant")
	return this->bryant_currentLocation;
	else if(s == "mary")
	return this->mary_currentLocation;
	else if(s == "tracy")
	return this->tracy_currentLocation;
	else if(s == "richard")
	return this->richard_currentLocation;
	else if(s == "otis")
	return this->otis_currentLocation;
	else if(s == "kayla")
	return this->kayla_currentLocation;
}