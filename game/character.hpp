#ifndef CHARACTER_H_
#define CHARACTER_H_
#include <stdio.h>

class Character{
private:
int currentLocation;
int knowledge;

public:
	Character();
	int getKnowledge();

	void setKnowledge();

	int getLocation();

	void setLocation(int newLocation);

	void saveKnowledge(int save);
};

#endif