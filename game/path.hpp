//--------------------------------//
// Group Project: SurviveTheNight //
// Authors: Bryan, Kylie, Rico    //
// File: Path.hpp	 	  //
//--------------------------------//

#include <iostream>
#include <string>
#include "items.hpp"
#include "suspects.hpp"

// Variables:
// 10 x 10 matrix hardcoded for edges (1 if connected, 0 if not)
// Each room in the house will be labeled with a vertex number and thats how 
// we will allow the player to traverse the map

//Functions:
class Path{
	private:
		Items item;
		Suspects suspect;
		std::vector<std::string> living = {"mug","poker","postcard","hammer"};
		std::vector<std::string> death = {"pen","letter","handkerchief","pocketwatch"};
		std::vector<std::string> kitchen = {"knife","water","plate"};
		std::vector<std::string> dining = {"cloth","spoon","fancy","letter"};
		std::vector<std::string> bathroom = {"towel","soap","toothbrush"};
		std::vector<std::string> lounge = {"phone","blanket","photo","clock"};
		std::vector<std::string> master = {"cane","candle","hat"};
		std::vector<std::string> guest = {"scarf","makeup","shoes"};

	public:
		int const pathMatrix[8][8] = {
		{0,1,1,1,1,1,0,0}, //living area 0 
		{1,0,0,0,0,0,0,0}, //death 1 
		{1,0,0,1,0,0,0,0}, //kitchen 2
		{1,0,1,0,0,0,0,0}, //dining area 3 
		{1,0,0,0,0,0,0,0}, //bathroom 4 
		{1,0,0,0,0,0,1,1}, // lounge 5
		{0,0,0,0,0,1,0,0}, //bedroom_1 6 
		{0,0,0,0,0,1,0,0}}; //bedroom_2 7

		bool checkConnections(int path1, int path2);

		bool validInspect(int room, std::string choice);

		bool validTake(int room, std::string choice);

		bool validInterview(int room, std::string choice);
			// 1 if connections exists, 0 if not

		std::vector<int> availablePaths(int location);
			// Prints all available paths from the current room
};
