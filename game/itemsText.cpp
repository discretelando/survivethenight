#include <iostream>
#include <map>
#include "items.hpp"

std::map<std::string, std::string> description = {
	{"mug", "the mug is bloodied\n"},
  {"poker", "blood splatter from the murder taking place only feet away from the fireplace\n"},
  {"postcard", "unsent postcard, to a \"Janet\"\n"},
  {"hammer", "clearly the murder weapon... still dripping with blood\n light enough to be wielded by man or woman\n"},
  {"pen", "an ordinary pen\n"},
  {"letter", ""},
  {"handkerchief", ""},
  {"pocketwatch", "shattered with an unfamiliar face in it\n"},
  {"knife", ""},
  {"water", "half drank glass of water\n"},
  {"plate", "this doesn't seem nearly as nice as the ones given out\n"},
  {"cloth", "a particlarly sharp knife has been stowed away inside of this cloth\n"},
  {"spoon", "a spoon fit for caviar\n"},
  {"fancy", "oof this looks very pretty\n"},
  {"letter", "letter kissed sealed... the from address is smeared\n but it came from, \"CA\"?\n"},
  {"towel", "hasn't been washed in atleast two weeks\n"},
  {"soap", "lavender! i bet whoever used this must have sily skin\n"},
  {"toothbrush", "an extra toothbrush?\n I thought he made it cleared he lived alone since his fiancee passed away\n"},
  {"phone", "a fairly basic phone, looks like an iphone?\n The screen is cracked it must be\n"},
  {"blanket", "a very cozy looking blanket, note to self, get more blankets for the apartment\n"},
  {"photo", "a photo, faced down\nof the host and his would be bethrothed\n"},
  {"clock", "a magnificent handcrafter alarm clock, must be swiss\n"},
  {"cane", "hmm i don't remember seeing the host with any encumberances\n"},
  {"candle", "a candle burned down to the wick, must have been a passionate night\n"},
  {"hat", "a missing hat from the hat rack\n"},
  {"scarf", "a very elegant scarf\nmust be worn on special outdoor occassions. Perhaps a funeral?\n"},
  {"makeup", "recently used makeup foundation, for a darker skin tone\n"},
  {"shoes", "pair of blood splattered two inch heels with one partially laced\n"}
};

void Items::itemText(std::string item) { 
    if(item == "weapon") {
       std::cout << description["weapon"];
    }
    else if(item == "mug") {
       std::cout << description["mug"];
    }
    else if(item == "pen") {
       std::cout << description["pen"];
    }
    else if(item == "poker") {
       std::cout << description["poker"];
    }
    else if(item == "postcard") {
       std::cout << description["postcard"];
    }
    else if(item == "hammer") {
       std::cout << description["hammer"];
    }
    else if(item == "letter") {
       std::cout << description["letter"];
    }
    else if(item == "handkerchief") {
       std::cout << description["handkerchief"];
    }
    else if(item == "pocketwatch") {
       std::cout << description["pocketwatch"];
    }
    else if(item == "knife") {
       std::cout << description["knife"];
    }
    else if(item == "water") {
       std::cout << description["water"];
    }
    else if(item == "plate") {
       std::cout << description["plate"];
    }
    else if(item == "cloth") {
       std::cout << description["cloth"];
    }
    else if(item == "spoon") {
       std::cout << description["spoon"];
    }
    else if(item == "fancy") {
       std::cout << description["fancy"];
    }
    else if(item == "letter") {
       std::cout << description["letter"];
    }
    else if(item == "towel") {
       std::cout << description["towel"];
    }
    else if(item == "soap") {
       std::cout << description["soap"];
    }
    else if(item == "toothbrush") {
       std::cout << description["toothbrush"];
    }
    else if(item == "phone") {
       std::cout << description["phone"];
    }
    else if(item == "blanket") {
       std::cout << description["blanket"];
    }
    else if(item == "photo") {
       std::cout << description["photo"];
    }
    else if(item == "clock") {
       std::cout << description["clock"];
    }
    else if(item == "cane") {
       std::cout << description["cane"];
    }
    else if(item == "candle") {
       std::cout << description["candle"];
    }
    else if(item == "hat") {
       std::cout << description["hat"];
    }
    else if(item == "scarf") {
       std::cout << description["scarf"];
    }
    else if(item == "makeup") {
       std::cout << description["scarf"];
    }
    else if(item == "shoes") {
       std::cout << description["scarf"];
    }
}

