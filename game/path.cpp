//--------------------------------//
// Group Project: SurviveTheNight //
// Authors: Bryan, Kylie, Rico    //
// File: Path.cpp				  //
//--------------------------------//

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "path.hpp"

// Variables:
// 10 x 10 matrix hardcoded for edges (1 if connected, 0 if not)
// Each room in the house will be labeled with a vertex number and thats how 
// we will allow the player to traverse the map

//Functions:
		bool Path::checkConnections(int path1, int path2){
			// 1 if connections exists, 0 if not
			if (pathMatrix[path1][path2] == 1){
				suspect.suspectsLocation(path2);
				return true;
			}
			return false;
		}

		bool Path::validInspect(int room, std::string choice){
			if(room == 0){
				if(std::find(this->living.begin(), this->living.end(), choice) != this->living.end())
					return true;
			}
			else if(room == 1){
				if(std::find(this->death.begin(), this->death.end(), choice) != this->living.end())
					return true;				
			}
			else if(room == 2){
				if(std::find(this->kitchen.begin(), this->kitchen.end(), choice) != this->living.end())
					return true;
			}
			else if(room == 3){
				if(std::find(this->dining.begin(), this->dining.end(), choice) != this->living.end())
					return true;
			}
			else if(room == 4){
				if(std::find(this->bathroom.begin(), this->bathroom.end(), choice) != this->living.end())
					return true;
			}
			else if(room == 5){
				if(std::find(this->lounge.begin(), this->lounge.end(), choice) != this->living.end())
					return true;
			}
			else if(room == 6){
				if(std::find(this->master.begin(), this->master.end(), choice) != this->living.end())
					return true;
			}
			else if(room == 7){
				if(std::find(this->guest.begin(), this->guest.end(), choice) != this->living.end())
					return true;
			}
			return false;
		}

		bool Path::validTake(int room, std::string choice){
			if(room == 0){
				if(std::find(this->living.begin(), this->living.end(), choice) != this->living.end()){
					this->living.erase(remove(this->living.begin(), this->living.end(), choice), this->living.end());
					return true;
				}
				else
					return false;
			}
			else if(room == 1){
				if(std::find(this->death.begin(), this->death.end(), choice) != this->death.end()){
					this->death.erase(remove(this->death.begin(), this->death.end(), choice), this->death.end());
					return true;
				}
			}
			else if(room == 2){
				if(std::find(this->kitchen.begin(), this->kitchen.end(), choice) != this->kitchen.end()){
					this->kitchen.erase(remove(this->kitchen.begin(), this->kitchen.end(), choice), this->kitchen.end());
					return true;
				}
				else
					return false;
			}
			else if(room == 3){
				if(std::find(this->dining.begin(), this->dining.end(), choice) != this->dining.end()){
					this->dining.erase(remove(this->dining.begin(), this->dining.end(), choice), this->dining.end());
					return true;
				}
				else
					return false;
			}
			else if(room == 4){
				if(std::find(this->bathroom.begin(), this->bathroom.end(), choice) != this->bathroom.end()){
					this->bathroom.erase(remove(this->bathroom.begin(), this->bathroom.end(), choice), this->bathroom.end());
					return true;
				}
				else
					return false;
			}
			else if(room == 5){
				if(std::find(this->lounge.begin(), this->lounge.end(), choice) != this->lounge.end()){
					this->lounge.erase(remove(this->lounge.begin(), this->lounge.end(), choice), this->lounge.end());
					return true;
				}
				else
					return false;
			}
			else if(room == 6){
				if(std::find(this->master.begin(), this->master.end(), choice) != this->master.end()){
					this->master.erase(remove(this->master.begin(), this->master.end(), choice), this->master.end());
					return true;
				}
				else
					return false;
			}
			else if(room == 7){
				if(std::find(this->guest.begin(), this->guest.end(), choice) != this->living.end()){
					this->living.erase(remove(this->living.begin(), this->living.end(), choice), this->living.end());
					return true;
				}
				else
					return false;
			}
			std::cout << "you're in the walls!!\n";
			return false;
		}

		bool Path::validInterview(int room, std::string choice){
			if(choice == "bryant" && room == suspect.getSuspectsLocation(choice))
				return true;
			else if(choice == "mary" && room == suspect.getSuspectsLocation(choice))
				return true;
			else if(choice == "tracy" && room == suspect.getSuspectsLocation(choice))
				return true;
			else if(choice == "richard" && room == suspect.getSuspectsLocation(choice))
				return true;
			else if(choice == "otis" && room == suspect.getSuspectsLocation(choice))
				return true;
			else if(choice == "kayla" && room == suspect.getSuspectsLocation(choice))
				return true;
			else{
				std::cout << "they're not here... try a ouija board... or text them\n";
				return false;
			}
		}

		std::vector<int> Path::availablePaths(int location){
			// Prints all available paths from the current room
			std::vector<int> returnVector;

			for (int i = 0;i < 8;i++){
				if (pathMatrix[location][i] == 1){
					returnVector.push_back(i);
				}
			}
			return returnVector;
		}
