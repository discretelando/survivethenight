#include <iostream>
#include <map>
#include<string>
#include "events.hpp"

//enum Event{kitchen, living_room = 10, murder = 20, dining = 30, bathroom = 40, stairs = 50};
std::map<std::string, std::string> events = {
	{"death", "Someone died here\n"},
	{"intro", 
	"Welcome to Survive the Night! A text-based murder-mystery game written all in C++.\n*FOR BEST EXPERIENCE PLAY IN FULL SCREEN*\nCredits: Ricardo Williams, Kyle Quan, and Bryan Carl\n\n\nA warm crackling fire illuminates the inside of a wooden Cabin. Good conversation and the smell of hot food swirl around the room.\nFriendly faces all around causes a feeling of complacency to overtake.\nAs the host begins to make a toast, all eight of you gather around the living room to hear his speech.\nYou hear the sounds of soft raining suddenly broken by falling thunderbolts. A flash of light sprints across the room and you hear a murderous scream!!!\nWhen the light fades you open your eyes to a body on the ground someone turns him over to reveal it was the host himself.\nMuffled murmours flood the room with an uneasiness. Everyone turns to you. What will you do?\n\n\n"},
	{"living", "Living Room: It's a warm and inviting room with shag carpeting from edge to edge.\n A round aburn rug adorns the middle of the floor with couches on either side of it.\nThe fire is still roaring with a fire stoker sitting beside it.\nA small table off to the left has some post cards, a mug, and a hammer on it.\n\n"},
	{"kitchen","A modern kitchen with hardwood floors and granite counter-tops.\nPots and pans hang from the ceiling over a center island with a sink.\nThere are knives safely tucked away in a knife block.\nTheres a plate sitting out by itself.\n\n"},
	{"lounge", "As you enter the Lounge, the floorboards creak showing the age of the house.\n You notice a couch off to the left with a blanket on it.\n The clock on the wall is ticking like a metronome, piercing the silence.\n To the right of the couch, there is a nightstand with an old phone and photos on it\n\n"},
	{"bathroom","You enter a neatly kept bathroom with most things tucked away.\nThe only things left out on the counter are soap, and two toothbrushes.\nThere is a toilet to the left of the counter and a shower behind it.\n\n"},
	{"dining", "A nicely seated dinner table strikes you as you walk in.\nSeated for seven people.\nA small bureau is to the left of the table, on it are pictures of relatives.\n"},
	{"master", "The master bedroom has a luxurious bear pelt adorning the mantle above the bed.\nDeer heads flank either side of the room, giving an ominous tone.\nThere are photos from past hunting trips on the walls.\nA single candle is lit, and hat hangs from the coat rack\n\n"},
	{"guest", "The guest bedroom has a nice rustic feel to it, with wooden carvings around the room.\nThere are pictures of family all around the walls, and a pair of shoes set neatly by the door.\nA single candle keeps the room lit, and the bed looks comfy.\n"},
	{"commands", "\nENTER \"print instructions\" at any time to print out all possible commands\n"},
	{"instructions", "*action noun* e.g. inspect wallet\n take: grabs a specified item\n\ninspect: gives details about a particular item in a room\n\ninterview: gives background information about a specified character\n\ndrop: drops a specified item from your inventory (you can only hold 15 at any given moment)\nenter: enters a specified room\n\nsave game: saves the game... who woulda thunk\n\n"},
	{"rooms", "\nENTER \"print rooms\" at any time to print out all available rooms from your current position\n"},
	{"commands_1", "BEWARE you can only accuse someone inside the death room...\nAND try to learn enough before attempting to do so... GOOD LUCK\n\n"}
};

void Events::printEvent(string event) { 
    if(event == "intro") {
       std::cout << events["intro"];
    }
    if(event == "living") {
       std::cout << events["living"];
    }
    if(event == "death") {
       std::cout << events["death"];
    }
    if(event == "kitchen") {
       std::cout << events["kitchen"];
    }
    if(event == "master") {
       std::cout << events["master"];
    }
    if(event == "guest") {
       std::cout << events["guest"];
    }
    if(event == "lounge") {
       std::cout << events["lounge"];
    }
    if(event == "bathroom") {
       std::cout << events["bathroom"];
    }
    if(event == "dining") {
       std::cout << events["dining"];
    }
    if(event == "commands") {
       std::cout << events["commands"];
    }
    if(event == "instructions") {
       std::cout << events["instructions"];
    }
    if(event == "commands_1") {
       std::cout << events["commands_1"];
    }
}
