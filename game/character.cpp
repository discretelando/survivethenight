#include <stdio.h>
#include "character.hpp"

Character::Character(){
	this->currentLocation = 0;
	this->knowledge = 0;
}

int Character::getKnowledge(){
	return this->knowledge;
}

void Character::setKnowledge(){
	this->knowledge = this->knowledge+1;
}

void Character::saveKnowledge(int save){
	this->knowledge = save;
}

int Character::getLocation(){
	return this->currentLocation;
}

void Character::setLocation(int newLocation){
	this->currentLocation = newLocation;
}