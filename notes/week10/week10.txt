## Survive the Night ##
  
# Authors: Rico, Kylie, Bryan #


This week we made all of our final pushes and commits for the project. We are quite proud of what we have accomplished over the past five weeeks.

Over the course of this week we improved the game engine so that it acually has terminating cases. The player needs to explore and have enough dialogue in order to actually solve the mystery

If enough knowledge is not developed over the course of the game, the player will be murdered and the game will end

We added a few more random events such as npcs moving around into different rooms. As well as added more realism to conversations by adding sleep to event printing.

Overall this week was the most successful week and it really showed in our score. We all learned a lot about vectors and matrix manipulations in order to get this project to work.

Not importing many or any external libraries helped us realize exactly what we were doing. All in all this week saw the most useful features added and we got a working product!